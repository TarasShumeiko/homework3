function Hamburger(size, stuffing) {
  this._size = size;
  this._stuffing = stuffing;
  this._price = size[0] + stuffing[0];
  this._calories = size[1] + stuffing[1];
  this._toppings = [];
}

Hamburger.SIZE_SMALL = [50, 20, "SIZE_SMALL"];
Hamburger.SIZE_LARGE = [100, 40, "SIZE_LARGE"];
Hamburger.STUFFING_CHEESE = [10, 20, "STUFFING_CHEESE"];
Hamburger.STUFFING_SALAD = [20, 5, "STUFFING_SALAD"];
Hamburger.STUFFING_POTATO = [15, 10, "STUFFING_POTATO"];
Hamburger.TOPPING_MAYO = [20, 5, "TOPPING_MAYO"];
Hamburger.TOPPING_SPICE = [15, 0, "TOPPING_SPICE"];

Hamburger.prototype.addTopping = function(topping) {
  if (topping[2] !== "TOPPING_MAYO" && topping[2] !== "TOPPING_SPICE") {
    return;
  }
  try {
    for (let i = 0; i <= this._toppings.length; i++) {
      if (this._toppings[i] === topping[2]) {
        this._price -= topping[0];
        this._calories -= topping[1];
        this._toppings[i] = "";
        throw new Error();
      }
    }
  } catch (e) {
    console.log(`duplicate topping: '${topping[2]}'`);
  }
  this._toppings.push(topping[2]);
  this._price += topping[0];
  this._calories += topping[1];
};

Hamburger.prototype.removeTopping = function(topping) {
  if (topping[2] !== "TOPPING_MAYO" && topping[2] !== "TOPPING_SPICE") {
    return;
  }
  try {
    for (let i = 0; i <= this._toppings.length; i++) {
      if (this._toppings[i] === topping[2]) {
        this._price -= topping[0];
        this._calories -= topping[1];
        this._toppings[i] = "";
        return;
      }
    }
    for (let i = 0; i <= this._toppings.length; i++) {
      if (this._toppings[i] !== topping[2]) {
        throw new Error();
      }
    }
  } catch (e) {
    console.log(`HamburgerException: '${topping[2]}' is already deleted`);
  }
};

Hamburger.prototype.getToppings = function() {
  let toppings = [];
  for (let i = 0; i <= this._toppings.length; i++) {
    if (this._toppings[i] === "TOPPING_MAYO") {
      toppings.push(this._toppings[i]);
    }
    if (this._toppings[i] === "TOPPING_SPICE") {
      toppings.push(this._toppings[i]);
    }
  }
  console.log(toppings);
};

Hamburger.prototype.getSize = function() {
  console.log(this._size[2]);
};

Hamburger.prototype.getStuffing = function() {
  console.log(this._stuffing[2]);
};

Hamburger.prototype.calculatePrice = function() {
  return this._price * 94;
};

Hamburger.prototype.calculateCalories = function() {
  return this._calories;
};

// function HamburgerException(name) {
//   console.log(`duplicate topping '${name}'`);
// };



let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);


console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.getToppings();
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
hamburger.getToppings();
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.getSize();
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
hamburger.getToppings();
hamburger.getStuffing();
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
